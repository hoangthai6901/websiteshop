<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class products extends Model
{
    use HasFactory;
    protected $table='products';
    protected $fillable =[
        'id',
        'category_id',
        'name',
        'slug',
        'small_description',
        'description',
        'material',
        'original_price',
        'selling_price',
        'image',
        'qty',
        'long',
        'width',
        'height',
        'brand_id',
        'status',
        'trending',
        'meta_title',
        'meta_descrip',
        'meta_keywords'
    ];
    public function getNamebrand(){
        return $this->belongsTo(Brands::class, 'brand_id','id');
    }
}
