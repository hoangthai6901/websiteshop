@extends('layouts.app')
@section('content')
    <table>
        <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Brand ID</th>
            <th>Price</th>
        </tr>
        @foreach($products as $product)
        <tr>
            <td>{{ $product->name }}</td>
            <td>{{ $product->description }}</td>
            <td>{{ $product->getNamebrand->nameBrand }}</td>
            <td>{{ $product->original_price }}</td>
        </tr>
        @endforeach
    </table>
@endsection
