import Vuex from 'vuex'
import AuthStore from './auth'
import ToastStore from './toast'
import MemberStore from './member'
import UserStore from './user'
import PointStore from './point'
import FacilityStore from './facility'
import PrefectureStore from './prefecture'
import CampaignStore from './campaign'
import CouponStore from './coupon'
import LangStore from './lang'
import VersionStore from './version'
import Vue from 'vue'
Vue.use(Vuex);

export default new Vuex.Store({
    state:{},
    mutations:{},
    action:{},
    modules:{
        auth:AuthStore,
        toast:ToastStore,
        member:MemberStore,
        user: UserStore,
        point:PointStore,
        facility: FacilityStore,
        prefecture:PrefectureStore,
        campaign:CampaignStore,
        coupon: CouponStore,
        lang:LangStore,
        version:VersionStore
    }
})