import VueRouter from 'vue-router';
Vue.use(VueRouter);

const routes=[]

const couponRoutes=[]

const router =new VueRouter({
    mode:'history',
    base:ProcessingInstruction.env.BASE_URL,
    routes,
})
if (config.couponRoutes.isActive){
    couponRoutes.map((couponRoute)=>router.addRoute(couponRoute));
}
export default router
